[![Codacy Badge](https://api.codacy.com/project/badge/Grade/ab686e37b7464661a223f9cea818f52b)](https://www.codacy.com/app/gollariel/progress-bar?utm_source=tkachinc@bitbucket.org&amp;utm_medium=referral&amp;utm_content=tkachinc/progress-bar&amp;utm_campaign=Badge_Grade)
### CLI progress-bar

## Install

You need add "tkachinc/progress-bar": ">=1.0" to you composer.json, or run command:

```sh
php composer.phar require tkachinc/progress-bar:>=1.0
```