<?php
namespace TkachInc\ProgressBar\Progress;

use TkachInc\CLI\Output\LinesManipulator;

/**
 * @author maxim
 */
class ProgressMultiLine
{
	protected $progress = [];
	protected $sort = [];

	/**
	 * @param ProgressBar $progress
	 */
	public function add(ProgressBar $progress)
	{
		$this->progress[] = $progress;
		$this->sort[] = $progress->getElements();
	}

	/**
	 * @return void
	 */
	public function sort()
	{
		array_multisort($this->sort, SORT_ASC, $this->progress, SORT_DESC, $this->progress);
	}

	/**
	 * @return void
	 */
	public function next()
	{
		reset($this->progress);
		reset($this->sort);
		/** @var ProgressBar $progress */
		foreach ($this->progress as $progress) {
			if (!$progress->isFinish()) {
				$progress->next();
			}
		}
	}

	/**
	 * @param array $search
	 * @param array $replace
	 * @param array ...$tools
	 */
	public function out($search = [], $replace = [], ...$tools)
	{
		reset($this->progress);
		reset($this->sort);

		$res = '';
		/** @var ProgressBar $progress */
		foreach ($this->progress as $progress) {
			$res .= $progress->out($search, $replace, false, ...$tools) . PHP_EOL;
		}

		fwrite(STDOUT, LinesManipulator::replaceMultiLine($res));
	}

	/**
	 * @param array $search
	 * @param array $replace
	 * @param array ...$tools
	 */
	public function display($search = [], $replace = [], ...$tools)
	{
		$this->next();
		$this->out($search, $replace, ...$tools);
	}

	/**
	 * @return array
	 */
	public function getProgress()
	{
		return $this->progress;
	}

	/**
	 * @param \Generator $generator
	 * @param callable $function
	 * @param \Closure $progressCallback
	 */
	public function worker(\Generator $generator, Callable $function, \Closure $progressCallback)
	{
		foreach ($generator as $item) {
			$res = '';
			/** @var ProgressBar $progress */
			foreach ($this->progress as $progress) {
				call_user_func_array($function, [$progress, $item]);
				$res .= call_user_func_array($progressCallback, [$progress]) . PHP_EOL;
			}
			if (!empty($res)) {
				fwrite(STDOUT, LinesManipulator::replaceMultiLine($res));
			}
		}
	}

	/**
	 * @param \Generator $generator
	 * @param callable $function
	 */
	public function displayDefaultBar(\Generator $generator, Callable $function)
	{
		/** @var ProgressBar $progress */
		foreach ($this->progress as $progress) {
			$progress->setFormat("_current_/_max_ [_bar_] %s%s _timeLeft_");
		}

		$generate = function (ProgressBar $progress) {
			$progress->next();

			return $progress->out(['_current_', '_max_', '_bar_', '_timeLeft_'], [$progress->getCurrent(), $progress->getElements(),
				sprintf("%'={$progress->getCurrentPercent()}s%'-{$progress->getDiff()}s", '>', ''), $progress->getTimeLeft()], false, '%');
		};

		$this->worker($generator, $function, $generate);
	}

	/**
	 * @param \Generator $generator
	 * @param callable $function
	 */
	public function displaySimpleBar(\Generator $generator, Callable $function)
	{
		/** @var ProgressBar $progress */
		foreach ($this->progress as $progress) {
			$progress->setFormat("%3d%%");
		}

		$generate = function (ProgressBar $progress) {
			return $progress->display([], [], false);
		};
		$this->worker($generator, $function, $generate);
	}
}