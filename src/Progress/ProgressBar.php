<?php
namespace TkachInc\ProgressBar\Progress;

use TkachInc\CLI\Output\LinesManipulator;

/**
 * Class ProgressBar
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ProgressBar
{
	protected $elements = 0;
	protected $usleep = 0;
	protected $format = "%3d%%";
	protected $current = 0;
	protected $percent = 100;
	protected $currentPercent = 0;
	protected $freq = 1;
	protected $startTime = 1;

	protected $secondLeft = 0;
	protected $timeLeft = 0;
	protected $speed = 0;
	protected $finish = null;

	/**
	 * @param        $elements
	 * @param int $usleep
	 * @param int $freq
	 * @param string $format
	 * @throws \Exception
	 */
	public function __construct($elements, $usleep = 10, $freq = 10, $format = "%3d%%")
	{
		if (!($elements > 0)) {
			$this->elements = 0;
		}

		$this->elements = $elements;
		$this->usleep = $usleep;
		$this->format = $format;
		$this->freq = max($freq, 1);
		$this->startTime = microtime(true);

		if ($this->elements > 1) {
			$this->finish = $this->elements;
		}
	}

	/**
	 * @param $format
	 */
	public function setFormat($format)
	{
		$this->format = $format;
	}

	/**
	 * @return bool
	 */
	public function isDisplay()
	{
		return ($this->current % $this->freq) === 0;
	}

	/**
	 * @param array $search
	 * @param array $replace
	 * @param bool $out
	 * @param array ...$tools
	 * @return null|string
	 */
	public function out($search = [], $replace = [], $out = true, ...$tools)
	{
		$result = sprintf(str_replace($search, $replace, $this->format), $this->currentPercent, ...$tools);
		if (($this->isDisplay() || $this->isFinish()) && $out) {
			fwrite(STDOUT, LinesManipulator::replaceSingleLine($result));
		}

		return $result;
	}

	/**
	 * @return bool
	 */
	public function isFinish()
	{
		if ($this->finish && $this->current >= $this->finish) {
			return true;
		}

		return false;
	}

	/**
	 * @return void
	 */
	public function next()
	{
		if ($this->isFinish()) {
//			$this->finish();
			return;
		}

		$this->current++;

		if ($this->current > $this->elements) {
			$this->elements = $this->current;
		}
		$this->currentPercent = ceil(($this->current * $this->percent) / $this->elements);

		$this->secondLeft = microtime(true) - $this->startTime;
		$this->timeLeft = ($this->secondLeft * $this->elements / $this->current) - $this->secondLeft;
		$this->speed = max(round($this->current / $this->secondLeft, 2), 0);

		if ($this->usleep > 0) {
			usleep($this->usleep);
		}

		return;
	}

	/**
	 * @return void
	 */
	public function finish()
	{
		if ($this->current > $this->elements) {
			$this->elements = $this->current;
		}
		$this->currentPercent = $this->percent;
		$this->timeLeft = 0;
		$this->speed = 0;
	}

	/**
	 * @param array $search
	 * @param array $replace
	 * @param bool $out
	 * @param array ...$tools
	 * @return null|string
	 */
	public function display($search = [], $replace = [], $out = true, ...$tools)
	{
		$this->next();

		return $this->out($search, $replace, $out, $tools);
	}

	/**
	 * @return int
	 */
	public function getCurrentPercent()
	{
		return $this->currentPercent;
	}

	/**
	 * @return int
	 */
	public function getMaxPercent()
	{
		return $this->percent;
	}

	/**
	 * @return int
	 */
	public function getSecondLeft()
	{
		return $this->secondLeft;
	}

	/**
	 * @return int
	 */
	public function getTimeLeft()
	{
		return $this->timeLeft;
	}

	/**
	 * @return int
	 */
	public function getSpeed()
	{
		return $this->speed;
	}

	/**
	 * @return int|mixed
	 */
	public function getFreq()
	{
		return $this->freq;
	}

	/**
	 * @return int
	 */
	public function getElements()
	{
		return $this->elements;
	}

	/**
	 * @return float
	 */
	public function getDiff()
	{
		$currentPercent = $this->getCurrentPercent();
		if ($currentPercent === 0) {
			$currentPercent = $currentPercent - 1;
		}

		return round($this->getMaxPercent() - $currentPercent, 2);
	}

	/**
	 * @return int
	 */
	public function getCurrent()
	{
		return $this->current;
	}

	/**
	 * @param \Generator $generator
	 * @param callable $function
	 * @param \Closure $progress
	 */
	public static function worker(\Generator $generator, Callable $function, \Closure $progress)
	{
		foreach ($generator as $item) {
			call_user_func_array($function, [$item]);
			call_user_func_array($progress, []);
		}
	}

	/**
	 * @param \Generator $generator
	 * @param callable $function
	 * @param $elements
	 * @param int $usleep
	 * @param int $freq
	 */
	public static function displayDefaultBar(\Generator $generator, Callable $function, $elements, $usleep = 10, $freq = 10)
	{
		$helper = new ProgressBar($elements, $usleep, $freq, "_current_/_max_ [_bar_] %s%s _timeLeft_");
		$generate = function () use ($helper) {
			$helper->next();
			$count = strlen($helper->getElements());
			$current = sprintf("%'. {$count}d", $helper->getCurrent());

			return $helper->out(['_current_', '_max_', '_bar_', '_timeLeft_'], [$current, $helper->getElements(),
				sprintf("%'={$helper->getCurrentPercent()}s%'-{$helper->getDiff()}s", '>', ''), round($helper->getTimeLeft(), 2)], true, '%');
		};
		static::worker($generator, $function, $generate);
	}

	/**
	 * @param \Generator $generator
	 * @param callable $function
	 * @param $elements
	 * @param int $usleep
	 * @param int $freq
	 */
	public static function displaySimpleBar(\Generator $generator, Callable $function, $elements, $usleep = 10, $freq = 10)
	{
		$helper = new ProgressBar($elements, $usleep, $freq);
		$generate = function () use ($helper) {
			return $helper->display();
		};
		static::worker($generator, $function, $generate);
	}
}